#pragma once
class Fraction
{
public:
	int num;
	int denom;

	Fraction() { num = 0; denom = 0; };
	friend void operator +(Fraction f1, Fraction f2);
	friend void operator -(Fraction f1, Fraction f2);
	friend void operator *(Fraction f1, Fraction f2);
	friend void operator /(Fraction f1, Fraction f2);
	friend bool operator == (Fraction f1, Fraction f2);
	friend bool operator != (Fraction f1, Fraction f2);
	friend bool operator > (Fraction f1, Fraction f2);
	friend bool operator < (Fraction f1, Fraction f2);
	friend bool operator >= (Fraction f1, Fraction f2);
	friend bool operator <= (Fraction f1, Fraction f2);
	friend void operator ++(Fraction f1);
	friend void operator --(Fraction f1);

};

